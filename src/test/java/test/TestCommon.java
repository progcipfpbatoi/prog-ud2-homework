/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import org.junit.After;
import org.junit.Before;

/**
 *
 * @author batoi
 */
public abstract class TestCommon {
    
    private PrintStream oldOut;
    private InputStream oldIn;
    protected ByteArrayOutputStream baos;
    
    @Before
    public void before() {
        // Redirigir salida
        baos = new ByteArrayOutputStream();
        PrintStream ps = new PrintStream(baos);
        oldOut = System.out;
        System.out.flush();
        System.setOut(ps);
    }
    
    protected void redirectInAndWriteInBuffer(String toWriteInBuffer) {
        InputStream inputStream = new ByteArrayInputStream(toWriteInBuffer.getBytes());
        BufferedInputStream in = new BufferedInputStream(inputStream);
        oldIn = System.in;
        System.setIn(in);
    }
    
    
    @After
    public void after() {
        // Reestablecer salida
        System.setOut(oldOut);
        System.setIn(oldIn);
    }
}
