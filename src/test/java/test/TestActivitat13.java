package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import prog.ud2.homework.Activitat13;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestActivitat13 extends TestCommon{

    @Test
    public void testCircleArea() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinalAreaCirculo = "954,40";

        assertTrue("El área del círculo no es correcta. Revisa tus operaciones  o el número de decimales mostrados en ella",
                        salidaSinBlancosNiSaltos.contains(resultadoFinalAreaCirculo));

    }

    @Test
    public void testCirclePerimeter() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinalPerimetroCirculo = "109,51";

        assertTrue("El perímetro final del cículo no es correcto. Revisa tus operaciones o el número de decimales mostrados en ella",
                        salidaSinBlancosNiSaltos.contains(resultadoFinalPerimetroCirculo));

    }

    @Test
    public void testTriangleArea() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinalAreaTriangulo = "132,13";

        assertTrue("El área del triángulo obtenida no es correcta. Revisa tus operaciones o el número de decimales mostrados en ella",
                        salidaSinBlancosNiSaltos.contains(resultadoFinalAreaTriangulo));

    }
    
    @Test
    public void testCircleRadiusTextExists() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String textoDebeAparecer = "Introdueix el valor del 'radi' d'un cercle:".replaceAll(" ", "");

        assertTrue("El texto de petición del radio no está correctamente escrito o no existe. Revisa que tu texto sea -> Introdueix el valor del 'radi' d'un cercle:",
                        salidaSinBlancosNiSaltos.contains(textoDebeAparecer));                        
    }

    @Test
    public void testTriangleBaseTextExists() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String textoDebeAparecer = "Ara introdueix el valor de la 'base' d'un triangle:".replaceAll(" ", "");

        assertTrue("El texto de petición de la base del triángulo no está correctamente escrito o no existe. Revisa que tu texto sea -> Ara introdueix el valor de la 'base' d'un triangle:",
                        salidaSinBlancosNiSaltos.contains(textoDebeAparecer));
    }


    @Test
    public void testTriangleHeightTextExists() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");
        Activitat13.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String textoDebeAparecer = "I finalment, la seua 'altura':".replaceAll(" ", "");

        assertTrue("El texto de petición de la altura del triángulo no está correctamente escrito o no existe. Revisa que tu texto sea -> I finalment, la seua 'altura':",
                        salidaSinBlancosNiSaltos.contains(textoDebeAparecer));
    }
    
    @Test
    public void testFormatCircleData() {


        // Redirigir entrada
        redirectInAndWriteInBuffer("17,43 21,45 12,32");

        Activitat13.main(null);
        String formatoSalidaDatosCirculo = "Cercle: Perímetre -> [\\d]+[,][\\d]{2} Àrea -> [\\d]+[,][\\d]{2}";
        
        String salidaFormateada = baos.toString().replaceAll("\n","").replaceAll(" ", "").toLowerCase();
        Pattern pat = Pattern.compile(formatoSalidaDatosCirculo.replaceAll(" ", "").toLowerCase());//[\\d]+x\\^2[+-]{1}[\\d]+x[+-]{1}[\\d]+=0x1=[\\d]+[,.][\\d]+x2=[+-]{1}[\\d]+[,.][\\d]+");
        Matcher mat = pat.matcher(salidaFormateada);

        assertTrue("El formato de la salida de los datos del círculo (perímetro y área) no es correcto.\nComprueba que has introducido el número de decimales indicado y que has respetado los errores ortográficos", mat.find());
    }
}

