package test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import prog.ud2.homework.Activitat14;


public class TestActivitat14 extends TestCommon {

    private void testResultYear(String anyo, String textoResultado) {
        Activitat14.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        
        assertTrue("La cantidad de kgs de basura en el año "+ anyo + " no es correcta o bien no has indicado el resultado para ese año. Revisa tus operaciones",
                        salidaSinBlancosNiSaltos.contains(anyo+":"+textoResultado));
    }

    @Test
    public void testResultYear2024() {
        testResultYear("2024", "463500");
    }

    @Test
    public void testResultYear2025() {
        testResultYear("2025", "427000");
    }

    @Test
    public void testResultYear2026() {
        testResultYear("2026", "390500");
    }

    @Test
    public void testResultYear2027() {
        testResultYear("2027", "354000");
    }

    @Test
    public void testResultYear2028() {
        testResultYear("2028", "317500");
    }
    
    @Test
    public void testHeader() {
        Activitat14.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String textoCabecera = "PROJECCIÓ DEL FEM EN REGNE UNIT".replaceAll(" ", "");;
        
        assertTrue("La cabecera de la salida mostrada no es correcta. Puede que hayas cometido alguna falta ortográfica",
                        salidaSinBlancosNiSaltos.startsWith(textoCabecera));
    }

    @Test
    public void testFormatResultYears() {

        testFormatYear("2024");
        testFormatYear("2025");
        testFormatYear("2026");
        testFormatYear("2027");
        testFormatYear("2028");
    }

    private void testFormatYear(String year) {

        Activitat14.main(null);
        String formatoSalidaDatosAnyo = "Any "+year+":[-?\\d]+ kgs";
        
        String salidaFormateada = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        Pattern pat = Pattern.compile(formatoSalidaDatosAnyo.replaceAll(" ", ""));
        Matcher mat = pat.matcher(salidaFormateada);

        assertTrue("No estás respetando el formato en el que se deben mostrar el año "+year+". Fíjate bien en el enunciado", mat.find());
    }
}

