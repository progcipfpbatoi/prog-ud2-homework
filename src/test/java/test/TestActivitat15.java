package test;

import org.junit.Test;


import static org.junit.Assert.assertTrue;
import prog.ud2.homework.Activitat15;

public class TestActivitat15 extends TestCommon {

    @Test
    public void testFinalResult1() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("13 3,7");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinal = "13ºC";

       assertTrue("Para una temperatura de 13 grados y velocidad de 3,7 kms/h el cálculo no es correcto. Comprueba que muestras la información sin decimales y acompañado de las unidades.",
                        salidaSinBlancosNiSaltos.contains(resultadoFinal));

    }

     @Test
    public void testFinalResult2() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("-5 13");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinal = "-10ºC";

       assertTrue("Para una temperatura de -5 grados y velocidad de 13 kms/h el cálculo no es correcto. Comprueba que muestras la información sin decimales y acompañado de las unidades.",
                        salidaSinBlancosNiSaltos.contains(resultadoFinal));

    }

    @Test
    public void testFinalResult3() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("0 30");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "");
        String resultadoFinal = "-6ºC";

       assertTrue("Para una temperatura de 0 grados y velocidad de 30 kms/h el cálculo no es correcto. Comprueba que muestras la información sin decimales y acompañado de las unidades.",
                        salidaSinBlancosNiSaltos.contains(resultadoFinal));

    }
    
    @Test
    public void testTemperatureRequestText() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("13 3,7");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "").toLowerCase();
        String textoSolicitud = "Introdueix la temperatura en ºC:".replaceAll(" ", "").toLowerCase();

       assertTrue("El texto de solicitud de la temperatura no es correcta. Revisa el texto de solicitud de este dato en el enunciado",
                        salidaSinBlancosNiSaltos.contains(textoSolicitud));

    }

     @Test
    public void testWindSpeedRequestText() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("13 3,7");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "").toLowerCase();
        String textoSolicitud = "Introdueix la velocitat del vent en km\\h:".replaceAll(" ", "").toLowerCase();

       assertTrue("El texto de solicitud de la velocidad del viento no es correcta. Revisa el texto de solicitud de este dato en el enunciado",
                        salidaSinBlancosNiSaltos.contains(textoSolicitud));

    }

     @Test
    public void testHeaderText() {
        // Redirigir entrada
        redirectInAndWriteInBuffer("13 3,7");
        Activitat15.main(null);
        String salidaSinBlancosNiSaltos = baos.toString().replaceAll("\n","").replaceAll(" ", "").toLowerCase();
        String textoCabecera = "CÀLCUL DE LA TEMPERATURA REAL".replaceAll(" ", "").toLowerCase();

       assertTrue("El texto de cabecera no es correcto. Revisa  en el enunciado el texto que debe aparecer",
                        salidaSinBlancosNiSaltos.contains(textoCabecera));

    }
}

